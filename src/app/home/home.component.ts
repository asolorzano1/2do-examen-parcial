import { Component, OnInit } from "@angular/core";
import { SseService } from 'src/app/services/sse/sse.service';
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  constructor(private sseService: SseService) {}
  ngOnInit() {
    this.sseService
      .getServerSentEvent("http://localhost:8082/raw")
      .subscribe(data => console.log(data));
  }
}
