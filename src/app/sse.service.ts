import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SseService {
  // @ts-ignore
  constructor(private _zone: ngZone) {}
  getServerSentEvent(url: string): Observable<any> {
    // @ts-ignore
    return Observable.create(observer => {
      const eventSource = this.getEventSource(url);
      eventSource.onmessage = event => {
        this._zone.run(() => {
          observer.next(event);
        });
      };
      eventSource.onerror = error => {
        this._zone.run(() => {
          observer.error(error);
        });
      };
    });
  }
  private getEventSource(url: string): EventSource {
    return new EventSource(url);
  }
}
